package main

var (
	infoQuestions = []string{
		// OS
		"1. Základní (von Neumanova) architektura počítače. Formát instrukce, instrukční sada. Princip činnosti současných počítačů OS a HW – procesor, registry, přerušeni a jeho správa, I/O operace, struktura a hierarchie paměti, DMA, základní deska.",
		"2. Výpočetní model a operační systémy (batch procesing, host/terminal,…cloud computing)",
		"3. Systémy a aplikace na platformě World Wide Web. Princip, činnost, správa. Značkovací, skriptovací a programovací jazyky, technologie na straně klienta/serveru.",
		"4. Služby OS a generické komponenty OS, správa procesů, správa operační paměti, I/O systému, správa vnější paměti, souborů, networking, ochrany, interpret příkazů, GUI.",
		"5. Architektura OS a principy výstavby OS, hierarchická vrstvená architektura, mikrojádro, modulová architektura, virtuální stroje",
		"6. Role operačního systému u současného počítače. Základní typy OS MULTITASKING, MULTITHREADING. Porovnání MS Windows a UNIX",
		"7. Operační systémy (macOS, Linux, iOS, Android, Symbian, Firmware, BIOS)",
		"8. Vestavěný systém (zabudovaný systém, embedded system)",
		"9. Souborové systémy, správa uživatelů, uživatelská práva, správa procesů, interprety v OS",

		"10. Síťové prvky, síťová řešení a jejich realizace, topologie sítí.",
		"11 Vlastnosti algoritmu. Strukturované algoritmy. Zápis algoritmu pomocí plošných strukturovaných diagramů.",
		"12. Řídící struktury. Podmínky if/else/else if. Větvení pomocí switch. Cyklus typu while. Cyklus typu do-while.",
		"13. Modulární programování. Funkce a parametry funkcí. Návratová hodnota funkce. Rekurze. Proměnné a jejich reprezentace v paměti. Ukazatele- pointry.",
		"14 Výroková logika. Výrok. Logická proměnná. Logické spojky a příklady použití. Výrokové formule.",
		"15. Statistické a dynamické datové struktury. Fronta, zásobník, spojový seznam, strom, graf – jejich význam a možná realizace v procedurálních i objektových programovacích jazycích.",

		// DB
		"16. Relační datový model, relační algebra, relačně databázová terminologie, Coddova pravidla",
		"17. Integrita dat, druhy integritních omezení, database triggers.",
		"18. Databázové transakce, smysl a principy, varianty řešení.",
		"19. Dotazovací jazyky, předpoklady a principy, SQL a QBE, standardizace SQL, kritika SQL.",
		"20. Relační a objektová databázová řešení, uložení dat, přístup k datům, ochrana dat.",
	}
)
