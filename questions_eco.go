package main

var (
	ecoQuestions = []string{
		// Ekonomie
		"1. Poptávka, nabídka (základní tvar, funkce, determinanty). Utváření rovnováhy na trhu, rovnovážný bod, tržní oscilace, renta výrobců a spotřebitelů. Elasticita poptávky, nabídky (druhy, co vyjadřuje).",
		"2. Základní a alternativní cíle firmy. Zisk účetní a ekonomický. Rozdíl mezi krátkým a dlouhým obdobím. Zisk v krátkém a dlouhém období.",
		"3. PRODUKČNÍ FUNKCE, NÁKLADOVÁ FUNKCE (CO VYJADŘUJÍ, MEZNÍ A PRŮMĚRNÉ VELIČINY). PODSTATA MAXIMALIZACE ZISKU, MAXIMALIZACE TRŽEB.",
		"4. Dokonalá, monopolistická konkurence (podstata, zisk v krátkém a dlouhém období) Vysvětlení pojmů oligolopol, duopol, monopol, monopson. Úřad pro ochranu hospodářské soutěže (význam, činnost). ",
		"5. Základní makroekonomické ukazatele. Hrubý domácí produkt, národní produkt, čistý produkt. Rozdíl mezi reálným a nominálním. Metody výpočtu HDP",
		"6. Inflace (podstata, druhy podle tempa růstu, měření). Míra nezaměstnanosti, druhy nezaměstnanosti podle příčin vzniku, dopady nezaměstnanosti, aktivní a pasivní politika zaměstnanosti.",
		"7. Peníze (vývojové stupně, formy a význam peněz). Fiskální a monetární politika (tvůrce, nástroje, cíle). Úloha centrální banky.",
		"8. Majetek podniku",
		"9. Kapitál podniku",
		"10. Výnosy, náklady a zisk podniku",
		"11. Kalkulace nákladů na výkony",
		"12. Investice v podnicích",
		"13. Mzdová soustava podniku",

		// Management
		"14. Charakteristika managementu, ...",
		"15. Organizační systém podniku, jeho dekompozice a vztahy mezi jeho subsystémy",
		"16. Organizační a řídící struktura podniku, jejich prvky, typy těchto struktur, řídící kapacita a faktory určující její velikost.",
		"17. Osobnost manažera, role manažera, manažerské funkce a jejich vymezení.",
		"18. Styl řízení, jeho základní typy, individuální styl řízení, vnitřní a vnější stránka stylu, princip delegovaní, jeho formální a obsahová stránka.",
		"19. Vedení lidí, Motivace",
		"20. Racionalizace řídící práce manažera, komunikace v organizaci, její význam, formy komunikace, komunikační kanály, komunikační modely",
	}
)
