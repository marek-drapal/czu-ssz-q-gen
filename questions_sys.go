package main

var (
	sysQuestions = []string{
		// SYS
		"1. Systémový přístup, systémová analýza a její kroky, definice systému",
		"2. Význam a postup matematického modelování, definice a struktura modelu",
		"3. Systémová dynamika",
		"4. Lineární optimalizační modely a jejich praktické aplikace.",
		"5. Dopravní a přiřazovací modely",
		"6. Teorie rozhodování, rozhodovací modely, modely teorie her",
		"7 Vícekriteriální rozhodování, vícekriteriální analýza variant, vícekriteriální optimalizace",

		// Projekt
		"8. Modely teorie grafu, minimální kostra, maximální tok, CPM, MPM, PERT",
		"9. Projektové řízení principy a nástroje",
		"10. Teorie omezení a lidský činitel v projektovém řízení",
		"11. Softwarové nástroje jako podpora v projektovém řízení",
		"12. Simulační modely a postupy",
		"13. Markovovské řetězce a modely teorie hromadné obsluhy",

		// Stat
		"14. NÁHODNÝ JEV, NÁHODNÁ VELIČINA. NÁHODNÉ JEVY, DEFINICE PRAVDĚPODOBNOSTI, VĚTY O SČÍTÁNÍ A NÁSOBENÍ PRAVDĚPODOBNOSTÍ. ČÍSELNÉ CHARAKTERISTIKY NÁHODNÉ VELIČINY, ROZDĚLENÍ NÁHODNÝCH VELIČIN",
		"15. Základní statistické charakteristiky, výběrové techniky, průzkumová analýza dat",
		"16. TEORIE ODHADU. ZÁKLADNÍ PRINCIPY, BODOVÝ ODHAD, INTERVALOVÝ ODHAD PRŮMĚRU ZÁKLADNÍHO SOUBORU, ROZPTYLU ZÁKLADNÍHO SOUBORU, PARAMETRU P ALTERNATIVNÍHO ROZDĚLENÍ",
		"17. Testování statistických hypotéz, parametrické testovací postupy",
		"18. Neparametrické testy",
		"19. Regresní a korelační analýza, jednoduchá lineární regrese a korelace, nelineární regrese a korelace, vícenásobná regrese a korelace, pořadová korelace",
		"20. Analýza časových řad, základní charakteristiky, analýza periodických a neperiodických časových řad",
	}
)
