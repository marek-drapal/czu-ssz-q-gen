# ČZU INFO BC SSZ Questions Generator
This program generated questions randomly per chosen field in terminal.

## Usage
### Win
To generate questions from "Ekonomie a řízení":

`./bin/win/ssz_q_gen.exe -qs "eco"`

To generate questions from "Kvantitativní metody":

`./bin/win/ssz_q_gen.exe -qs "sys"`

To generate questions from "Výpočetní sýstemy a informační inženýrství":

`./bin/win/ssz_q_gen.exe -qs "info"`

### Linux / Mac
To generate questions from "Ekonomie a řízení":

`./bin/unix/ssz_q_gen -qs "eco"`

To generate questions from "Kvantitativní metody":

`./bin/unix/ssz_q_gen -qs "sys"`

To generate questions from "Výpočetní sýstemy a informační inženýrství":

`./bin/unic/ssz_q_gen -qs "info"`
