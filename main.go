package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"
)

type questionType string

const (
	typeEco  questionType = "eco"
	typeSys  questionType = "sys"
	typeInfo questionType = "info"
)

func main() {
	qTypeFlag := flag.String("qs", "eco", "Type of questions to generate")
	flag.Parse()

	qType := questionType(*qTypeFlag)

	questions := getQuestions(qType)

	count := len(questions)
	for j := 0; j < count; j++ {
		s := rand.NewSource(time.Now().UnixNano())
		r := rand.New(s)
		i := r.Intn(len(questions))

		fmt.Print(questions[i] + "\n")
		_, _ = fmt.Scanf("Continue")

		questions[i] = questions[len(questions)-1]
		questions[len(questions)-1] = ""
		questions = questions[:len(questions)-1]
	}

	fmt.Println("Iterated through all provided questions...")
}

func getQuestions(qType questionType) []string {
	switch qType {
	case typeEco:
		return ecoQuestions
	case typeSys:
		return sysQuestions
	case typeInfo:
		return infoQuestions
	default:
		fmt.Println("Unknown question type")
		return []string{}
	}
}
